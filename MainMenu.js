var game;

BasicGame.MainMenu = function (game) {
	this.music = null;
	this.playButton = null;
	
	this.title = null;
	this.getready = null
	this.counter = 0 ;
	this.step = Math.PI * 2 / 360 ;
};

BasicGame.MainMenu.prototype = {

	create: function () {
		this.music = this.add.audio('titleMusic');
		this.music.volume = 0.2;
		this.music.play();

		this.background = this.add.sprite(0, 0, 'bg_starfield');
		
		this.title = this.add.sprite(0, 0, 'title');
		this.title.alpha = 0;
		this.title.rotation = -0.1;
		this.title.x = this.game.width / 2;
		this.title.anchor.x = this.title.anchor.y = 0.5;
		this.game.add.tween(this.title).to({alpha: 0.8}, 2000, Phaser.Easing.Linear.None, true, 1000);

		this.startButton = this.add.button(this.game.width / 2, this.game.height - 100, 'startButton', this.startGame, this);
		this.startButton.scale.setTo(0.25);
		this.startButton.anchor.set(0.5);
	},

	update: function () {
		this.titleWaves();
	},
	
	titleWaves: function() {
		// move title up and down smoothly
		var tStep = Math.sin(this.counter);
		this.title.y = (this.game.height / 2) + (tStep * 30);
		this.title.rotation += Phaser.Math.degToRad(0.1 * tStep);
		this.counter += this.step;
	},

	startGame: function (pointer) {
		this.music.stop();
		this.state.start('Game');
	},
	
	render: function () {
		//this.game.debug.spriteInfo(this.title, 32, 32);
	}

};
