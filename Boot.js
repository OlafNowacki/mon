var BasicGame = {};

BasicGame.Boot = function (game) {

};

BasicGame.Boot.prototype = {

    init: function () {

        //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
        this.input.maxPointers = 1;

        //  automatically pause the game if the browser tab loses focus
        this.stage.disableVisibilityChange = true;

        if (this.game.device.desktop)
        {
            //  desktop specific settings
            this.scale.pageAlignHorizontally = true;
        }
        else
        {
            //  mobile settings
            //  scale the game, no lower than 480x260 and no higher than 1024x768
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.setMinMax(480, 260, 1220, 768);
            this.scale.forceLandscape = true;
            this.scale.pageAlignHorizontally = true;
        }

    },

    preload: function () {

        //  load assets required for preloader 
        this.load.image('bg_starfield', 'images/bg_starfield.jpg');

    },

    create: function () {

        //  By this point the preloader assets have loaded to the cache, we've set the game settings
        //  start the preloader
        this.state.start('Preloader');

    }

};
