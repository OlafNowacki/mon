BasicGame.Game = function (game) {
    this.game;
    this.add;
    this.camera;
    this.cache;
    this.input;
    this.load;
    this.math;
    this.sound;
    this.stage;
    this.time;
    this.tweens;
    this.state;
    this.world;
    this.particles;
    this.physics;
    this.rnd;
};

BasicGame.Game.prototype = {
	
	sliders: {},		
	slidersOutput: {},
	spendingDepartments: ["ship", "def", "ind", "eco", "tec"],
	shipTypes: ["scout", "colony"], // "corvette", "frigate", "destroyer", "cruiser", 
	data: {"selected": null, "stardate": 2200},
	styles: {
		"title": {font: "24px 'Press Start 2P'", fill: "#d3a624", align: "center"}, 
		"yellow": {font: "12px 'Press Start 2P'", fill: "#ffdf51", align: "center"},
		"blue": {font: "16px zelda", fill: "#7196be", align: "center"}
	},

    create: function () {
		var background = this.add.sprite(0, 0, 'bg_starmap');
		var foreground = this.add.sprite(0, 0, 'frame_starmap');
		
		this.shipyard = this.add.group();
		this.shipyard.x = 778;
		this.shipyard.y = 480;
		var buttonShipType = this.add.button(50, 0, 'scout', this.changeShipType, this);
		buttonShipType.scale.setTo(3);
		buttonShipType.anchor.set(0.5);
		buttonShipType.name = "shipIcon";
		this.shipyard.add(buttonShipType)
		var textShipType = this.add.text(50, 50, "scout", this.styles.yellow, this.shipyard);
		textShipType.name = "shipText";
		textShipType.anchor.x = 0.45;
		

		var dock = this.add.sprite(768, 556, 'dock');
		dock.scale.setTo(2);

		var buttonNextTurn = this.add.button(1070, 720, 'button_nextTurn', this.nextTurn, this);
		
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		
		this.stars = this.add.group();
		this.urknall();
		
		this.corona = this.add.sprite(0, 0, 'corona');
		this.corona.anchor.set(0.5);
		this.corona.scale.setTo(0.125, 0.125);
		
		this.ships_in_transit = this.add.group();
		
		this.makePlanetInfoPanel();

		this.startingStar();
				
		this.makeSpendingPanel();
		
		this.addShip(this.data.selected, "colony");
		this.addShip(this.data.selected, "scout");
		this.addShip(this.data.selected, "scout");
    },

	urknall: function () {
		var star_names = ['Aldebaran', 'Altair', 'Andromeda', 'Antares', 'Aries', 'Betelgeuse', 'Capella',
		                  'Cassiopeia', 'Cepheus', 'Ceti', 'Corona Borealis', 'Corvus', 'Crux', 'Cè', "Galos",
		                  'Cóng Guān', 'Dagobah', 'Deneb IV', 'Draco', 'Endor', 'Fomalhout', 'Fū Lц', 'Fū',
		                  'Gemini', 'Gliese', 'Grus', 'HR 7703', 'HR 8832', 'Hoth', 'Jūn Nán Mén', 'Kepler',
		                  'Kessel', 'Kimah', 'Leporis', 'Lyra', 'Mezzaroth', 'Mintaka', 'Mu Delphi', 'Naboo', 'Nachash',
		                  'Nū Shї', 'Orion', 'Pavonis', 'Pegasus', 'Pisces', 'Polaris', 'Pollux',
		                  "Przybylski's Star", 'Puppis', 'Rigel', 'Rì', 'Sol', 'Tatooine', 'Tau Ceti',
		                  'Tài Yáng Shσu', 'Tài Zї', 'Ursa Major', 'Van Maanen', 'Vega', 'Vela', 'Vulcan',
		                  'Vulpecula', 'Xerxes', 'Xiāng', 'Zuσ Xiá'];
		this.star_types = ["TERRESTRIAL", "JUNGLE", "OCEAN", "STEPPE", "ARID", "ICE", "BARREN", "DEAD", "TOXIC", "RADIATED"];
		
		for (i = 0; i < star_names.length; i++) {
			this.stars.add(this.addStar(star_names[i]));
		}
	},

	addStar: function (name) {	
		/* check distance */
		do {
			var x = randGauss(2, 94) * 8;
			var y = randGauss(2, 94) * 8;
			var all_stars = this.stars.getAll();
			var toClose = false;
			for (i = 0; i < all_stars.length; i++) {
				var s2 = all_stars[i];
				if (this.math.distance(x, y, s2.x, s2.y) <= 40) {
					toClose = true;
					break;
				};
			};
		} while (toClose);
		
		var star = this.add.sprite(x, y, "unexplored");
		star.anchor.set(0.5);
		star.scale.setTo(0.125, 0.125);
		star.name = name;
		star.renderable = false;

		//star.data = {};
		star.data.name = "unexplored";
		star.data.type = this.rnd.weightedPick(this.star_types);
		star.data.max_pop = this.rnd.integerInRange(2, 22) * 5;
		star.data.special = this.rnd.weightedPick(["", "RICH", "POOR", "VERY RICH", "ARTEFACTS", "VERY POOR"]);
		star.data.population = 0;
		star.data.factories = 0;
		star.data.production = 0;
		star.data.waste = 0;
		star.data.bases = 0;
		star.data.shield = 0;
		star.data.explored = false;
		star.data.owned = false;
		star.data.buildShip = "scout";
		
		star.data.spending = {
			"ship": {"value": 0, "lock": false},
			"def": {"value": 0, "lock": false},
			"ind": {"value": 1, "lock": false},
			"eco": {"value": 0, "lock": false}, //"eco": {"value": 0, "lock": true},
			"tec": {"value": 0, "lock": false}
		};
		
		star.data.ships = this.add.group();
		star.data.ships.x = 778;
		star.data.ships.y = 566;
		star.data.ships.width = 432;
		star.data.ships.height = 144;
		star.data.ships.visible = false;
		
		star.events.onInputOver.add( function() {
			//this.game.debug.text(star.data.name, star.x + 20, star.y);
		}, this);
		star.events.onInputDown.add( function() {
			this.selectStar(star);
			this.infoStar(this.data.selected);
		}, this);
		
		return star;
	},
	
	makePlanetInfoPanel: function () {
		this.planetInfo = this.add.group();
		this.planetColony = this.add.group();
		this.planetSpending = this.add.group();
		this.star_info = {
			"name": this.add.text(800, 20, "", this.styles.title, this.planetInfo), 
			"type": this.add.text(800, 70, "", this.styles.yellow, this.planetInfo), 
			"special": this.add.text(800, 95, "", this.styles.blue, this.planetInfo),
			"max_pop": this.add.text(800, 120, "", this.styles.blue, this.planetInfo),
			
			"population": this.add.text(800, 150, "", this.styles.yellow, this.planetColony),
			"bases": this.add.text(1000, 150, "", this.styles.yellow, this.planetColony),
			
			"factories": this.add.text(800, 180, "", this.styles.yellow, this.planetSpending),
			"waste": this.add.text(1000, 180, "", this.styles.yellow, this.planetSpending),			
			"production": this.add.text(800, 200, "", this.styles.yellow, this.planetSpending)
		};		
		this.infoStardate = this.add.text(20, 20, this.data.stardate, this.styles.blue);
	},
	
	makeSpendingPanel: function () {
 		this.world.add(slickUI.container.displayGroup);
		this.spendingPanel = new SlickUI.Element.Panel(778, 230, 432, 180);
       	slickUI.add(this.spendingPanel);
		this.makeSpendingSlider("ship", 10);
		this.makeSpendingSlider("def", 40);
		this.makeSpendingSlider("ind", 70);
		this.makeSpendingSlider("eco", 100);
		this.makeSpendingSlider("tec", 130);
	},
	
	makeSpendingSlider: function (department, y) {
		var label = new SlickUI.Element.Text(10, y, department);
		this.spendingPanel.add(label);
		this.sliders[department] = new SlickUI.Element.Slider(60, y + 14, 256, 0.0);
		this.spendingPanel.add(this.sliders[department]);
		this.sliders[department].onDrag.add(function (value) {
			this.BasicGame.Game.prototype.changeSpending(this.BasicGame.Game.prototype.data.selected, department, value);
		});
		this.slidersOutput[department] = new SlickUI.Element.Text(350, y, 'None');
		this.spendingPanel.add(this.slidersOutput[department]);
	},

	startingStar: function () {
		var star = this.stars.getByName("Sol");
		star.renderable = true;
		star.inputEnabled = true;
		star.data.type = "TERRESTRIAL";
		star.data.max_pop = 100;
		star.data.special = "";
		star.data.population = 50;
		star.data.factories = 30;
		star.data.production = 55;
		star.data.owned = true;
		this.exploreStar(star);
		this.selectStar(star);
		this.showStarsInReach(star, 80);
	},
	
	addShip: function (star, type) {
		var ship = this.add.sprite(0, 0, type, null, star.data.ships);
		ship.anchor.set(0.5);
		ship.scale.setTo(2);
		ship.data = {
			"dockX": 0,
			"dockY": 0,
			"destination": null,
			"type": type,
			"range": 30,
			"speed": 10
		};
		this.reorderDock(star);
		
		ship.inputEnabled = true;
		ship.input.enableDrag(true);
		ship.events.onDragStart.add(
			function (ship, pointer) {
				ship.scale.setTo(1);
			}, this);
		ship.events.onDragUpdate.add(
			function (ship, pointer) {
				this.lookAt(ship, this.data.selected, ship);
			}, this);
		ship.events.onDragStop.add(this.onDragStop, this);
		this.game.physics.arcade.enable(ship);
	},
	
	lookAt: function (sprite, from, to) {
		sprite.rotation = this.game.physics.arcade.angleBetween(to, from, this.world) - 1.57;
	},
	
	onDragStop: function (ship, pointer) {
		var dropOn = this.game.physics.arcade.overlap(ship, this.stars, this.dropShipOnStar, null, this);
		if (!dropOn) {
			ship.scale.setTo(2);
			ship.x = ship.data.dockX;
			ship.y = ship.data.dockY;
			ship.rotation = 0;
		};
	},
	
	dropShipOnStar: function (ship, star) {
		console.log(ship.data.type + " ship departs to " + star.name);
		this.ships_in_transit.add(ship);
		ship.x = this.data.selected.x;
		ship.y = this.data.selected.y;
		ship.data.destination = star;
		star.data.ships.remove(ship);
		ship.inputEnabled = false;
		this.reorderDock(this.data.selected);
	},
	
	exploreStar: function (star) {
		star.data.explored = true;
		star.data.name = star.name;
		star.loadTexture(star.data.type);
	},
	
	showStarsInReach: function (star, reach) {
		var all_stars = this.stars.getAll();
		for (i = 0; i < all_stars.length; i++) {
			var s2 = all_stars[i];
			if (star.name != s2.name) {
				var dist = this.math.distance(star.x, star.y, s2.x, s2.y);
				if (dist <= reach) {
					s2.renderable = true;
					s2.inputEnabled = true;
					this.game.physics.arcade.enable(s2);
				};
			};
		};
	},
	
	selectStar: function (star) {
		this.corona.x = star.x;
		this.corona.y = star.y;
		if (this.data.selected != null) {
			this.data.selected.data.ships.visible = false;
		};
		this.data.selected = star;
		star.data.ships.visible = true;
	},
	
	changeSpending: function (star, department, value) {
		console.log(star.name, department, value);
		//var star = this.data.selected;
		var diff = star.data.spending[department].value - value;
		var distributeToDepartments = [];
//		var max = 1;
		// find responding sliders
		for (i = 0; i < this.spendingDepartments.length; i++) {
			var key = this.spendingDepartments[i];
			if (key != department && ! star.data.spending[key].lock) {
				if (diff > 0 || star.data.spending[key].value > 0) {
					distributeToDepartments.push(key);
				};
//			} else {
//				max -= star.data.spending[k].value;
			};
		};

		star.data.spending[department].value = value;
		for (i = 0; i < distributeToDepartments.length; i++) {
			key = distributeToDepartments[i];
			var dept = star.data.spending[key];
			dept.value += diff / distributeToDepartments.length;
			if (dept.value < 0) {
				value -= dept.value;
				dept.value = 0;
			};
			this.sliders[key].setValue(dept.value);
		};
		this.setSpendingOutputs(star);
	},
	
	setSpendingOutputs: function (star) {
		var production = star.data.production;
				for (i = 0; i < this.spendingDepartments.length; i++) {
					var key = this.spendingDepartments[i];
					var text = this.slidersOutput[key];		
					var dept = star.data.spending[key];
					var credits = production * dept.value;
					text.value = cutFloat(dept.value, 2);
					if (key == "ship") {
						text.value = cutFloat(credits, 0) + " BC";
					} else if (key == "def") {
						text.value = cutFloat(credits, 0) + " BC";
					} else if (key == "ind") {
						text.value = cutFloat(credits / 10, 1) + "/y";
					} else if (key == "eco") {
						if (credits < star.data.factories / 2) {
							text.value = "Waste";
						} else { 
							text.value = "Clean";
						};
					} else if (key == "tec") {
						text.value = cutFloat(credits, 0) + " BC";
					};
				};
	},

	infoStar: function (star) {
		var star = this.data.selected;
		var info = this.star_info;
		this.star_info.name.text = star.name;
		this.star_info.type.text = star.data.type + " PLANET";
		this.star_info.special.text = star.data.special;
		this.star_info.max_pop.text = "POP " + star.data.max_pop + " MAX";
		this.star_info.population.text = "POP " + Math.floor(star.data.population);
		this.star_info.bases.text = "BASES " + star.data.bases;
		this.star_info.factories.text = "FACTORIES " + Math.floor(star.data.factories);
		this.star_info.waste.text = "WASTE " + Math.floor(star.data.waste);
		this.star_info.production.text = "PRODUCTION " + Math.floor(star.data.production);
		this.sliders.ship.setValue(star.data.spending.ship.value);
		this.sliders.def.setValue(star.data.spending.def.value);
		this.sliders.ind.setValue(star.data.spending.ind.value);
		this.sliders.eco.setValue(star.data.spending.eco.value);
		this.sliders.tec.setValue(star.data.spending.tec.value);
		this.setShipyardIcon(star.data.buildShip);
		
		if (star.data.owned) {
			this.planetColony.visible = true;
			this.planetSpending.visible = true;
			this.shipyard.visible = true;
			this.spendingPanel.x = 778;
		} else if (star.data.explored) {
			this.planetColony.visible = true;
			this.planetSpending.visible = false;
			this.shipyard.visible = false;
			this.spendingPanel.x = 1778;
		} else {
			this.planetColony.visible = false;
			this.planetSpending.visible = false;
			this.shipyard.visible = false;
			this.spendingPanel.x = 1778;
			this.star_info.name.text = "UNEXPLORED";
			this.star_info.type.text = "";
			this.star_info.special.text = "";
			this.star_info.max_pop.text = "";
		};
		this.setSpendingOutputs(star);
	},
	
    update: function () {
		//this.infoStar(this.data.selected);
    },

	nextTurn: function () {
		this.data.stardate += 1;
		this.infoStardate.text = this.data.stardate;
		
		/* star growth */
		// todo: race & planet modifiers!
		var all_stars = this.stars.getAll();
		for (i = 0; i < all_stars.length; i++) {
			var star = all_stars[i];
			
			star.data.waste += star.data.factories * 1.0;
			
			if (star.data.production > 0) {
				console.log(i, star.name, star.data.factories / 2 / star.data.production);
//				debugger;
//				this.changeSpending(star, "eco", star.data.factories / 2 / star.data.production);
			}			
			star.data.waste -= star.data.production * star.data.spending.eco.value * 2;
			if (star.data.waste < 0) {
				star.data.waste = 0;
			}
			// tech
			// ship
			// def
			star.data.factories += star.data.production * star.data.spending.ind.value / 10;
			if (star.data.factories > star.data.max_pop * 2) {
				star.data.factories = star.data.max_pop * 2;
				star.data.spending.tec.value = star.data.spending.ind.value;
				star.data.spending.ind.value = 0;
			};
			star.data.production = star.data.population * 0.5 + star.data.factories;
			/* population growth */
			var x = star.data.population / star.data.max_pop;
			if (x > 0.0 && x < 1.0) {
				star.data.population *= 0.1 * Math.pow(x - 1.35, 2) + 1.0;
			};
			if (star.data.population > star.data.max_pop) {
				star.data.population = star.data.max_pop;
			};

			//star.data.spending.eco.value = (star.data.waste / 2) / star.data.production;
		};	
		this.infoStar(this.data.selected);
		
		this.moveShips();
	},
	
	changeShipType: function () {
		var star = this.data.selected;
		var ship = star.data.buildShip;
		var i = this.shipTypes.indexOf(ship) + 1;
		if (i >= this.shipTypes.length) {
			i = 0;
		};
		ship = this.shipTypes[i]
		star.data.buildShip = ship;
		this.setShipyardIcon(ship);
	},
	
	setShipyardIcon: function (ship) {
		this.shipyard.getByName("shipIcon").loadTexture(ship);
		this.shipyard.getByName("shipText").text = ship;
	},
	
	moveShips: function () {
		var ships_in_transit = this.ships_in_transit.getAll();
		for (i = 0; i < ships_in_transit.length; i++) {
			var ship = ships_in_transit[i];
			var speed = ship.data.speed;
			var dest = ship.data.destination;
			this.lookAt(ship, ship, dest);
			if (this.math.distance(ship.x, ship.y, dest.x, dest.y) <= speed) {
				var landing = this.game.add.tween(ship)
				landing.to({
					x: dest.x, 
					y: dest.y}, 500, Phaser.Easing.Linear.None, true);
				landing.onComplete.add(this.arrival, this);
			} else {
				var vx = Math.round(speed * Math.cos(ship.rotation - 1.57));
				var vy = Math.round(speed * Math.sin(ship.rotation - 1.57));
				this.game.add.tween(ship).to({
					x: ship.x + vx, 
					y: ship.y + vy }, 500, Phaser.Easing.Linear.None, true);
			};
		};
	},
	
	reorderDock: function (star) {
		for (i = 0; i < star.data.ships.length; i++) {
			var ship = star.data.ships.getAt(i);
			ship.x = i * 72 + 36;
			ship.y = 36;
			ship.data.dockX = ship.x;
			ship.data.dockY = ship.y;
		};
	},
	
	arrival: function (ship) {
		var star = ship.data.destination;
		ship.scale.setTo(2);
		ship.rotation = 0;
		ship.inputEnabled = true;
		this.ships_in_transit.remove(ship);
		if (ship.data.type == "colony" && ! star.data.owned) {
			console.log("colonize " + star.name);
			this.exploreStar(star);
			star.data.owned = true;
			star.data.population = 2;
			this.showStarsInReach(star, 80);
			ship.destroy();
		} else if (! star.data.explored) {
			console.log("explore " + star.name);
			this.exploreStar(star);
			star.data.ships.add(ship);	
		} else {
			star.data.ships.add(ship);
		};
		this.reorderDock(star);
	},

    quitGame: function (pointer) {
        //  Here you should destroy anything you no longer need.
        //  Stop music, delete sprites, purge caches, free resources, all that good stuff.

        //  Then let's go back to the main menu.
        this.state.start('MainMenu');
    },

	render: function () {
		//game.debug.text("Over: " + bunny.input.pointerOver(), 32, 32);
		//game.debug.text(game.input.mouse.locked, 320, 32);
	}

};
