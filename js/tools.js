function randInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
};

function randGauss (min, max) {
	var min = min / 2;
	var max = max / 2;
	return Math.round(randInt(min, max) + randInt(min, max));
};

function cutFloat (value, decimals) {
	var pow = Math.pow(10, decimals);
	return Math.round(value * pow) / pow;
};