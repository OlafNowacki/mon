
BasicGame.Preloader = function (game) {
	this.background = null;
	this.ready = false;
};

BasicGame.Preloader.prototype = {

	preload: function () {

		this.background = this.add.sprite(0, 0, 'bg_starfield');
 
		this.load.image('title', 'images/title.png');
		this.load.image('startButton', 'images/start.png');
		this.load.image('button_nextTurn', 'images/button_nextTurn.png');
		this.load.audio('titleMusic', ['audio/Sara_Afonso_-_02_-_The_Cavern.mp3']);

		this.load.image("bg_starmap", "images/bg_starmap.png");
		this.load.image("frame_starmap", "images/frame_starmap.png");

		this.load.image("corona", "images/planets/corona.png");
		this.load.image("TERRESTRIAL", "images/planets/terrestrial.png");
		this.load.image("JUNGLE", "images/planets/jungle.png");
		this.load.image("OCEAN", "images/planets/ocean.png");
		this.load.image("STEPPE", "images/planets/steppe.png");
		this.load.image("ARID", "images/planets/arid.png");
		this.load.image("ICE", "images/planets/ice.png");
		this.load.image("BARREN", "images/planets/barren.png");
		this.load.image("DEAD", "images/planets/dead.png");
		this.load.image("TOXIC", "images/planets/toxic.png");
		this.load.image("RADIATED", "images/planets/radiated.png");
		this.load.image("unexplored", "images/planets/unex_128.png");

		this.load.image("scout", "images/ships/scout.png");
		this.load.image("colony", "images/ships/colony.png");
		this.load.image("dock", "images/ships/dock.png");

		//this.preloadBar = this.add.sprite(310, 64, 'title');
		//this.load.setPreloadSprite(this.preloadBar);
		//this.load.bitmapFont('minecraftia', 'fonts/minecraftia-black.png', 'fonts/minecraftia.xml');
		
		slickUI = this.game.plugins.add(Phaser.Plugin.SlickUI);
		slickUI.load('ui/kenney.json');		
	
	},

	create: function () {

		// Once load has finished we disable the crop 
		// because we're going to sit in the update loop for a short while as the music decodes
		//this.preloadBar.cropEnabled = false;

	},

	update: function () {

		//	You don't actually need to do this, but I find it gives a much smoother game experience.
		//	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
		//	You can jump right into the menu if you want and still play the music, but you'll have a few
		//	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
		//	it's best to wait for it to decode here first, then carry on.
		
		//	If you don't have any music in your game then put the game.state.start line into the create function and delete
		//	the update function completely.
		
		if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
		{
			this.ready = true;
			this.state.start('MainMenu');
		}

	}

};
