universe
	actors
		name
		race
		planets
			owned
			explored
		spending
			ships
			bases
			spying
			security
		income
			trade
			planets
		saving (%)
		reserve
		cost
			factories
			waste
		tech
			computers
				ecm jammer I					100
				deep space scanner				400
				battle comp II 					620
				robotic controls III 			1600
				battle comp III 				2500	
				ecm jammer III 					3600
				improved space scanner 			4220
				battle comp IV 					5620		
			construction
				industrial tech 9				
				waste 80 % 						620
				industrial tech 8				1600
				duralloy armor 					2500
			force_fields
				deflector shields II			240
				deflector shields III			1500
				planetary shield v				2160
				repulsor beam					3840
				deflector shields IV			6000
			planetology
				barren environment				180
				improved eco restauration		500
				terraforming +20				1280
				dead environment				1620
				death spores 					2000
				inferno environment				2880
				enhanced eco restauration		3380
				toxic environment 				4000
			propulsion
				hydrogen fuel cells (range 4) 	180
				deuterium fuel cells (range 5)	500
				nuclear engines (2 parsecs) 	720
				irridium fuel cells (range 6) 	1620
				inertial stabilizer 			2000
				sub-light drives (3 parsecs)	2880
				dotomite crystals (range 7) 
				energy pulsar					5120
				fusion drives (4 parsecs)		6480
			weapons
				hand lasers						100
				hyper-v rockets					400
				gatling laser 					620
				anti-missile rockets 			900
				hyper-x rockets 				1600
				ion cannon 						2500
				merculite missiles 				4900
				neutron blaster 				5620
			--
			scanner range (3)
			robot controls (2/col)
			factory waste (100%)
			ground combat (0%)
			terraform (0)
			waste elemination (2/BC)
			ship range (3)
			ship speed (1)
			
	stars
		star
			data
				name
				type
				max_pop
				special
				population
				factories
				waste
				bases
				shield
				explored	bool
				owned		bool
				spending
					ship
						value
						lock
					def
						value
						lock
					ind
						value
						lock
					eco
						value
						lock
					tec
						value
						lock
				ships
				
ship	
	type
	size
	space
	space_avail
	name
	computer
		attack_level
	shield
	ecm
		missle_def
	armor
		hp
	engine
		warp
		def
	maneuver
		combat_speed
	weapon1-4
		type
		count
	special1-3
	cost
	maintenance_cost

		cost	space	avail	hp		ecm		def
small	6		40		38		3		3		3
medium	39		204		189		18		2		2
large	220		1020	920		100		1		1
huge	1340	5100	4400	600		0		0
		
weapon
	name 
	description
	damage
	cost
	size
	power
	space
	range

special
	name
	description
	cost
	size
	power
	space


	reserve fuel tanks
	standard colony base
	battle scanner

				